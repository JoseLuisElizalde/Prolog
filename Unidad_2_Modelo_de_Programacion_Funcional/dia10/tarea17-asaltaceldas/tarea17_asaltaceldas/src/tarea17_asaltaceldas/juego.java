/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea17_asaltaceldas;

import java.awt.Color;

/**
 *
 * @author eskaboot
 */
public class juego extends javax.swing.JFrame {

    String matriz[][]=new String[9][9];
    
    public juego() {
        initComponents();
        matriz[0][0]="O";
        cerocero.setText(""+matriz[0][0]);
        generar_rateros();
        
        
    }
    
    public void generar_rateros(){
        //Siempre van a ser 3 rateros
        int rateros=0;
        
        
        while(rateros<3){
            int n=(int)(Math.random()*7)+2;
            int m=(int)(Math.random()*7)+2;
            
            switch(n){
                case 2:
                    switch(m){
                        case 2:
                            matriz[2][2]="X";
                            dosdos.setText(matriz[2][2]);
                        break;
                        case 3:
                            matriz[2][3]="X";
                            dostres.setText(matriz[2][3]);
                        break;
                        case 4:
                            matriz[2][4]="X";
                            doscuatro.setText(matriz[2][4]);
                        break;
                        case 5:
                            matriz[2][5]="X";
                            doscinco.setText(matriz[2][5]);
                        break;
                        case 6:
                            matriz[2][6]="X";
                            dosseis.setText(matriz[2][6]);
                        break;
                        case 7:
                            matriz[2][7]="X";
                            dossiete.setText(matriz[2][7]);
                        break;
                        case 8:
                            matriz[2][8]="X";
                            dosocho.setText(matriz[2][8]);
                        break;
                    }
                break;
                case 3:
                    switch(m){
                        case 2:
                            matriz[3][2]="X";
                            tresdos.setText(matriz[3][2]);
                        break;
                        case 3:
                            matriz[3][3]="X";
                            trestres.setText(matriz[3][3]);
                        break;
                        case 4:
                            matriz[3][4]="X";
                            trescuatro.setText(matriz[3][4]);
                        break;
                        case 5:
                            matriz[3][5]="X";
                            trescinco.setText(matriz[3][5]);
                        break;
                        case 6:
                            matriz[3][6]="X";
                            tresseis.setText(matriz[3][6]);
                        break;
                        case 7:
                            matriz[3][7]="X";
                            tressiete.setText(matriz[3][7]);
                        break;
                        case 8:
                            matriz[3][8]="X";
                            tresocho.setText(matriz[3][8]);
                        break;
                    }
                    
                break;
                case 4:
                    switch(m){
                        case 2:
                            matriz[4][2]="X";
                            cuatrodos.setText(matriz[4][2]);
                        break;
                        case 3:
                            matriz[4][3]="X";
                            cuatrotres.setText(matriz[4][3]);
                        break;
                        case 4:
                            matriz[4][4]="X";
                            cuatrocuatro.setText(matriz[4][4]);
                        break;
                        case 5:
                            matriz[4][5]="X";
                            cuatrocinco.setText(matriz[4][5]);
                        break;
                        case 6:
                            matriz[4][6]="X";
                            cuatroseis.setText(matriz[4][6]);
                        break;
                        case 7:
                            matriz[4][7]="X";
                            cuatrosiete.setText(matriz[4][7]);
                        break;
                        case 8:
                            matriz[4][8]="X";
                            cuatroocho.setText(matriz[4][8]);
                        break;
                    }
                    
                break;
                case 5:
                    switch(m){
                        case 2:
                            matriz[5][2]="X";
                            cincodos.setText(matriz[5][2]);
                        break;
                        case 3:
                            matriz[5][3]="X";
                            cincotres.setText(matriz[5][3]);
                        break;
                        case 4:
                            matriz[5][4]="X";
                            cincocuatro.setText(matriz[5][4]);
                        break;
                        case 5:
                            matriz[5][5]="X";
                            cincocinco.setText(matriz[5][5]);
                        break;
                        case 6:
                            matriz[5][6]="X";
                            cincoseis.setText(matriz[5][6]);
                        break;
                        case 7:
                            matriz[5][7]="X";
                            cincosiete.setText(matriz[5][7]);
                        break;
                        case 8:
                            matriz[5][8]="X";
                            cincoocho.setText(matriz[5][8]);
                        break;
                    }
                break;
                case 6:
                    switch(m){
                        case 2:
                            matriz[6][2]="X";
                            seisdos.setText(matriz[6][2]);
                        break;
                        case 3:
                            matriz[6][3]="X";
                            seistres.setText(matriz[6][3]);
                        break;
                        case 4:
                            matriz[6][4]="X";
                            seiscuatro.setText(matriz[6][4]);
                        break;
                        case 5:
                            matriz[6][5]="X";
                            seiscinco.setText(matriz[6][5]);
                        break;
                        case 6:
                            matriz[6][6]="X";
                            seisseis.setText(matriz[6][6]);
                        break;
                        case 7:
                            matriz[6][7]="X";
                            seissiete.setText(matriz[6][7]);
                        break;
                        case 8:
                            matriz[6][8]="X";
                            seisocho.setText(matriz[6][8]);
                        break;
                    }
                break;
                case 7:
                    switch(m){
                        case 2:
                            matriz[7][2]="X";
                            sietedos.setText(matriz[7][2]);
                        break;
                        case 3:
                            matriz[7][3]="X";
                            sietetres.setText(matriz[7][3]);
                        break;
                        case 4:
                            matriz[7][4]="X";
                            sietecuatro.setText(matriz[7][4]);
                        break;
                        case 5:
                            matriz[7][5]="X";
                            sietecinco.setText(matriz[7][5]);
                        break;
                        case 6:
                            matriz[7][6]="X";
                            sieteseis.setText(matriz[7][6]);
                        break;
                        case 7:
                            matriz[7][7]="X";
                            sietesiete.setText(matriz[7][7]);
                        break;
                        case 8:
                            matriz[7][8]="X";
                            sieteocho.setText(matriz[7][8]);
                        break;
                    }
                    
                break;
                case 8:
                    switch(m){
                        case 2:
                            matriz[8][2]="X";
                            ochodos.setText(matriz[8][2]);
                        break;
                        case 3:
                            matriz[8][3]="X";
                            ochotres.setText(matriz[8][3]);
                        break;
                        case 4:
                            matriz[8][4]="X";
                            ochocuatro.setText(matriz[8][4]);
                        break;
                        case 5:
                            matriz[8][5]="X";
                            ochocinco.setText(matriz[8][5]);
                        break;
                        case 6:
                            matriz[8][6]="X";
                            ochoseis.setText(matriz[8][6]);
                        break;
                        case 7:
                            matriz[8][7]="X";
                            ochosiete.setText(matriz[8][7]);
                        break;
                        case 8:
                            matriz[8][8]="X";
                            ochoocho.setText(matriz[8][8]);
                        break;
                    }
                break;
            }
            
            System.out.println(n+"-"+m);
            
            rateros++;
        }
        
        
    }//End method generar_rateros
    
    public void cargar_matriz(){
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        cerocero.setText(""+matriz[0][0]);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        cerocero = new javax.swing.JButton();
        cerouno = new javax.swing.JButton();
        cerodos = new javax.swing.JButton();
        cerotres = new javax.swing.JButton();
        cerocuatro = new javax.swing.JButton();
        cerocinco = new javax.swing.JButton();
        ceroseis = new javax.swing.JButton();
        cerosiete = new javax.swing.JButton();
        ceroocho = new javax.swing.JButton();
        unocero = new javax.swing.JButton();
        unouno = new javax.swing.JButton();
        unodos = new javax.swing.JButton();
        unotres = new javax.swing.JButton();
        unocuatro = new javax.swing.JButton();
        unocinco = new javax.swing.JButton();
        unoseis = new javax.swing.JButton();
        unosiete = new javax.swing.JButton();
        unoocho = new javax.swing.JButton();
        doscero = new javax.swing.JButton();
        dosuno = new javax.swing.JButton();
        dosdos = new javax.swing.JButton();
        dosocho = new javax.swing.JButton();
        doscinco = new javax.swing.JButton();
        doscuatro = new javax.swing.JButton();
        dosseis = new javax.swing.JButton();
        dostres = new javax.swing.JButton();
        dossiete = new javax.swing.JButton();
        tresdos = new javax.swing.JButton();
        tressiete = new javax.swing.JButton();
        trescinco = new javax.swing.JButton();
        tresocho = new javax.swing.JButton();
        trescero = new javax.swing.JButton();
        trestres = new javax.swing.JButton();
        tresuno = new javax.swing.JButton();
        trescuatro = new javax.swing.JButton();
        tresseis = new javax.swing.JButton();
        cuatrocero = new javax.swing.JButton();
        cuatrotres = new javax.swing.JButton();
        cuatroocho = new javax.swing.JButton();
        cuatrocinco = new javax.swing.JButton();
        cuatrosiete = new javax.swing.JButton();
        cuatrouno = new javax.swing.JButton();
        cuatrodos = new javax.swing.JButton();
        cuatroseis = new javax.swing.JButton();
        cuatrocuatro = new javax.swing.JButton();
        cincocero = new javax.swing.JButton();
        cincouno = new javax.swing.JButton();
        cincosiete = new javax.swing.JButton();
        cincoseis = new javax.swing.JButton();
        cincocinco = new javax.swing.JButton();
        cincocuatro = new javax.swing.JButton();
        cincodos = new javax.swing.JButton();
        cincotres = new javax.swing.JButton();
        cincoocho = new javax.swing.JButton();
        seiscero = new javax.swing.JButton();
        seisuno = new javax.swing.JButton();
        seisdos = new javax.swing.JButton();
        seistres = new javax.swing.JButton();
        seiscuatro = new javax.swing.JButton();
        seiscinco = new javax.swing.JButton();
        seisseis = new javax.swing.JButton();
        seissiete = new javax.swing.JButton();
        seisocho = new javax.swing.JButton();
        sieteseis = new javax.swing.JButton();
        sieteocho = new javax.swing.JButton();
        sietecinco = new javax.swing.JButton();
        sietetres = new javax.swing.JButton();
        sietecero = new javax.swing.JButton();
        sietecuatro = new javax.swing.JButton();
        sietedos = new javax.swing.JButton();
        sietesiete = new javax.swing.JButton();
        sieteuno = new javax.swing.JButton();
        ochocero = new javax.swing.JButton();
        ochouno = new javax.swing.JButton();
        ochodos = new javax.swing.JButton();
        ochotres = new javax.swing.JButton();
        ochocuatro = new javax.swing.JButton();
        ochocinco = new javax.swing.JButton();
        ochoseis = new javax.swing.JButton();
        ochosiete = new javax.swing.JButton();
        ochoocho = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 153, 153));

        doscero.setToolTipText("");

        cincoocho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cincoochoActionPerformed(evt);
            }
        });

        seisocho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seisochoActionPerformed(evt);
            }
        });

        sieteocho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sieteochoActionPerformed(evt);
            }
        });

        ochoocho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ochoochoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(doscero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dosuno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dosdos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dostres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(doscuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(doscinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dosseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dossiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dosocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(trescero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tresuno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tresdos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(trestres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(trescuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(trescinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tresseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tressiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tresocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cuatrocero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatrouno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatrodos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatrotres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatrocuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatrocinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatroseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cuatrosiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatroocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cincocero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincouno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincodos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincotres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincocuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincocinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincoseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cincosiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincoocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(seiscero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seisuno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seisdos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seistres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seiscuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seiscinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seisseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seissiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seisocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(sietecero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sieteuno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sietedos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sietetres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sietecuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sietecinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sieteseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sietesiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sieteocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ochocero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochouno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochodos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochotres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochocuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochocinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochoseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ochosiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochoocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cerocero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerouno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerodos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerotres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerocuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerocinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ceroseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cerosiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ceroocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(unocero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unouno, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unodos, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unotres, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unocuatro, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unocinco, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unoseis, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unosiete, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(unoocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(ceroocho, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cerodos, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                            .addComponent(cerotres, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                            .addComponent(cerocuatro, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                            .addComponent(cerocinco, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                            .addComponent(ceroseis, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                            .addComponent(cerosiete, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE))
                        .addComponent(cerouno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE))
                    .addComponent(cerocero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(unoocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(unodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(unotres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(unocuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(unocinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(unoseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(unosiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(unocero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(unouno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(dosdos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dostres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(doscuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(doscinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dosseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dossiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dosocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(doscero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dosuno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tresdos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(trestres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(trescuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(trescinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tresseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tressiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tresocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(trescero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tresuno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cuatrodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatrotres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatrocuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatrocinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatroseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatrosiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cuatroocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cuatrocero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cuatrouno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cincodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincotres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincocuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincocinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincoseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincosiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cincoocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cincocero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cincouno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(seisdos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seistres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seiscuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seiscinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seisseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seissiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(seisocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(seiscero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(seisuno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(sietedos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sietetres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sietecuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sietecinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sieteseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sietesiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sieteocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(sietecero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sieteuno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(ochodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochotres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochocuatro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochocinco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochoseis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochosiete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ochoocho, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ochocero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ochouno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Destino");

        jLabel2.setText("X:");

        jLabel3.setText("Y:");

        jButton1.setText("Generar ");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField1))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel1))
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void cincoochoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cincoochoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cincoochoActionPerformed

    private void seisochoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seisochoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_seisochoActionPerformed

    private void sieteochoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sieteochoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sieteochoActionPerformed

    private void ochoochoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ochoochoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ochoochoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        
            int a=Integer.parseInt(jTextField1.getText());
            int b=Integer.parseInt(jTextField2.getText());
            
            switch(a){
                case 2:
                    switch(b){
                        case 2:
                            matriz[2][2]="D";
                            dosdos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[2][3]="D";
                            dostres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[2][4]="D";
                            doscuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[2][5]="D";
                            doscinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[2][6]="D";
                            dosseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[2][7]="D";
                            dossiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[2][8]="D";
                            dosocho.setBackground(Color.green);
                        break;
                    }
                break;
                case 3:
                    switch(b){
                        case 2:
                            matriz[3][2]="D";
                            tresdos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[3][3]="D";
                            trestres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[3][4]="D";
                            trescuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[3][5]="D";
                            trescinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[3][6]="D";
                            tresseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[3][7]="D";
                            tressiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[3][8]="D";
                            tresocho.setBackground(Color.green);
                        break;
                    }
                    
                break;
                case 4:
                    switch(b){
                        case 2:
                            matriz[4][2]="D";
                            cuatrodos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[4][3]="D";
                            cuatrotres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[4][4]="D";
                            cuatrocuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[4][5]="D";
                            cuatrocinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[4][6]="D";
                            cuatroseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[4][7]="D";
                            cuatrosiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[4][8]="D";
                            cuatroocho.setBackground(Color.green);
                        break;
                    }
                    
                break;
                case 5:
                    switch(b){
                        case 2:
                            matriz[5][2]="D";
                            cincodos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[5][3]="D";
                            cincotres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[5][4]="D";
                            cincocuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[5][5]="D";
                            cincocinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[5][6]="D";
                            cincoseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[5][7]="D";
                            cincosiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[5][8]="D";
                            cincoocho.setBackground(Color.green);
                        break;
                    }
                break;
                case 6:
                    switch(b){
                        case 2:
                            matriz[6][2]="D";
                            seisdos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[6][3]="D";
                            seistres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[6][4]="D";
                            seiscuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[6][5]="D";
                            seiscinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[6][6]="D";
                            seisseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[6][7]="D";
                            seissiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[6][8]="D";
                            seisocho.setBackground(Color.green);
                        break;
                    }
                break;
                case 7:
                    switch(b){
                        case 2:
                            matriz[7][2]="D";
                            sietedos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[7][3]="D";
                            sietetres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[7][4]="D";
                            sietecuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[7][5]="D";
                            sietecinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[7][6]="D";
                            sieteseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[7][7]="D";
                            sietesiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[7][8]="D";
                            sieteocho.setBackground(Color.green);
                        break;
                    }
                    
                break;
                case 8:
                    switch(b){
                        case 2:
                            matriz[8][2]="D";
                            ochodos.setBackground(Color.green);
                        break;
                        case 3:
                            matriz[8][3]="D";
                            ochotres.setBackground(Color.green);
                        break;
                        case 4:
                            matriz[8][4]="D";
                            ochocuatro.setBackground(Color.green);
                        break;
                        case 5:
                            matriz[8][5]="D";
                            ochocinco.setBackground(Color.green);
                        break;
                        case 6:
                            matriz[8][6]="D";
                            ochoseis.setBackground(Color.green);
                        break;
                        case 7:
                            matriz[8][7]="D";
                            ochosiete.setBackground(Color.green);
                        break;
                        case 8:
                            matriz[8][8]="D";
                            ochoocho.setBackground(Color.green);
                        break;
                    }
                break;
            }
        jButton1.setBackground(Color.green);
    }//GEN-LAST:event_jButton1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new juego().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cerocero;
    private javax.swing.JButton cerocinco;
    private javax.swing.JButton cerocuatro;
    private javax.swing.JButton cerodos;
    private javax.swing.JButton ceroocho;
    private javax.swing.JButton ceroseis;
    private javax.swing.JButton cerosiete;
    private javax.swing.JButton cerotres;
    private javax.swing.JButton cerouno;
    private javax.swing.JButton cincocero;
    private javax.swing.JButton cincocinco;
    private javax.swing.JButton cincocuatro;
    private javax.swing.JButton cincodos;
    private javax.swing.JButton cincoocho;
    private javax.swing.JButton cincoseis;
    private javax.swing.JButton cincosiete;
    private javax.swing.JButton cincotres;
    private javax.swing.JButton cincouno;
    private javax.swing.JButton cuatrocero;
    private javax.swing.JButton cuatrocinco;
    private javax.swing.JButton cuatrocuatro;
    private javax.swing.JButton cuatrodos;
    private javax.swing.JButton cuatroocho;
    private javax.swing.JButton cuatroseis;
    private javax.swing.JButton cuatrosiete;
    private javax.swing.JButton cuatrotres;
    private javax.swing.JButton cuatrouno;
    private javax.swing.JButton doscero;
    private javax.swing.JButton doscinco;
    private javax.swing.JButton doscuatro;
    private javax.swing.JButton dosdos;
    private javax.swing.JButton dosocho;
    private javax.swing.JButton dosseis;
    private javax.swing.JButton dossiete;
    private javax.swing.JButton dostres;
    private javax.swing.JButton dosuno;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JButton ochocero;
    private javax.swing.JButton ochocinco;
    private javax.swing.JButton ochocuatro;
    private javax.swing.JButton ochodos;
    private javax.swing.JButton ochoocho;
    private javax.swing.JButton ochoseis;
    private javax.swing.JButton ochosiete;
    private javax.swing.JButton ochotres;
    private javax.swing.JButton ochouno;
    private javax.swing.JButton seiscero;
    private javax.swing.JButton seiscinco;
    private javax.swing.JButton seiscuatro;
    private javax.swing.JButton seisdos;
    private javax.swing.JButton seisocho;
    private javax.swing.JButton seisseis;
    private javax.swing.JButton seissiete;
    private javax.swing.JButton seistres;
    private javax.swing.JButton seisuno;
    private javax.swing.JButton sietecero;
    private javax.swing.JButton sietecinco;
    private javax.swing.JButton sietecuatro;
    private javax.swing.JButton sietedos;
    private javax.swing.JButton sieteocho;
    private javax.swing.JButton sieteseis;
    private javax.swing.JButton sietesiete;
    private javax.swing.JButton sietetres;
    private javax.swing.JButton sieteuno;
    private javax.swing.JButton trescero;
    private javax.swing.JButton trescinco;
    private javax.swing.JButton trescuatro;
    private javax.swing.JButton tresdos;
    private javax.swing.JButton tresocho;
    private javax.swing.JButton tresseis;
    private javax.swing.JButton tressiete;
    private javax.swing.JButton trestres;
    private javax.swing.JButton tresuno;
    private javax.swing.JButton unocero;
    private javax.swing.JButton unocinco;
    private javax.swing.JButton unocuatro;
    private javax.swing.JButton unodos;
    private javax.swing.JButton unoocho;
    private javax.swing.JButton unoseis;
    private javax.swing.JButton unosiete;
    private javax.swing.JButton unotres;
    private javax.swing.JButton unouno;
    // End of variables declaration//GEN-END:variables
}
