;Ejercicio 1
(DEFUN militar()
    (print "Ingresa la edad: ")
    (setf a (read))
    (print "Ingresa el nivel de sisben: ")
    (setf b (read))
    (calculo_cobro a b)
)

(defun calculo_cobro (a b)
    
    (unless (< a 19)
        (print "Eres mayor de 18 a;os!!")
        (if(= b 1)
            (FORMAT T "Tienes un descuento del 40% y tendras que pagar: ~D" (* 350000 0.4))
        )
        (if(= b 2)
            (FORMAT T "Tienes un descuento del 30% y tendras que pagar: ~D" (* 350000 0.3))
        )
        (if(= b 3)
            (FORMAT T "Tienes un descuento del 15% y tendras que pagar: ~D" (* 350000 0.15))

        )
        (if(/= b 1 2 3)
            (FORMAT T "No tienes descuento y tendras que pagar: ~D" 350000)
        )
    )
    (unless (>= a 19)
        (print "Eres menor o tienes 18 a;os!!")
        
        (if(= b 1)
            (FORMAT T "Tienes un descuento del 60% y tendras que pagar: ~D" (* 200000 0.6))
        )
        (if(= b 2)
            (FORMAT T "Tienes un descuento del 40% y tendras que pagar: ~D" (* 200000 0.4))
        )
        (if(= b 3)
            (FORMAT T "Tienes un descuento del 20% y tendras que pagar: ~D" (* 200000 0.2))

        )
        (if(/= b 1 2 3)
            (print "No tienes descuento!! :( ")
        )
    )

 )




;Ejercicio 2
(DEFUN operario()
    (print "Ingresa el numero de tornillos defectuosos: ")
    (setf a (read))
    (print "Ingresa el numero de tornillos producidos: ")
    (setf b (read))
    (calculo_eficiencia a b)
)

(defun calculo_eficiencia (a b)
    (if(AND (< a 200) (> b 10000)) 
        (print "Eres grado 8! Felicidades!!")
    )
    (if(AND (> a 200) (> b 10000)) 
        (print "Eres grado 7! Casi perfecto!")
    )
    (if(AND (< a 200) (< b 10000)) 
        (print "Eres grado 6! Ahi vas we!")
    )
    (if(AND (> a 200) (< b 10000)) 
        (print "Eres grado 5! No mams! >:|  !!")
    )

 )


;Ejercicio 3
(DEFUN minuscula()
    (print "Ingresa una letra minuscula: ")
    (setf a (read))
    (elegir a)
)

(defun elegir (a)
(CASE a
    ('a 'vocal)
    ('e 'vocal)
    ('i 'vocal)
    ('o 'vocal)
    ('u 'vocal)
    ('y 'semivocal)
    (T 'consonante) ) )


;Ejercicio 4

(defun fibonacci()
    (print "Ingresa el numero positivo a calcular: ")
    (setf b (read))
    (calculofib b)
)
(defun calculofib (b)
    (cond ((equal b 0) 1)
    ((equal b 1) 1)
    (t (+ (fib (- b 1))
    (fib (- b 2)))))
)