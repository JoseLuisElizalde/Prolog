;;Deacuerdo al documento adjunto "if", hacer los ejercicios correspondientes
;;Con todas las condicionales: if, when, unless

;;Problema 1 - if
(defun problema1if(a)
    (if (> a 10)
        (print  "Tiene un aumento del 10%")
    )
    (if (and (< a 10)  (> a 5))
        (print "Tiene un aumento del 7%")
        
    )
    (if (and (< a 5) (> a 3))
        (print "Tiene un aumento del 5%")
        
    )
    (if (< a 3)
        (print "Tiene un aumento del 3%")
        
    )
)

(problema1if 6)

;;Problema 1 - when
(defun problema1when(b)
    (when (> a 10) 
        (print "Tieene un aumento del 10%")
    )
    (when (and (< a 10) (> a 5))
        (print "Tiene un aumento del 7%")
    )
    (when (and (< a 5) (> a 3))
        (print "Tiene un aumento del 5%")
    )
    (when (< a 3)
        (print "Tiene un aumento del 3%")
    )

)

