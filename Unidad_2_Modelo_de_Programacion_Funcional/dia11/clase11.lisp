;Ejemplo unless, generalmente se hace "la negacion de la verdad"
;Dentro del if todo se va a ejecutar, funciona como un progn en ese sentido
(defun test(a)
    (unless (/= a a)
    (print "Hola1")
    (print "Hola2")
)
)

;Ejemplo de if basico
(defun test2(a)
    (if (= a a)
        (print "Hola 1")
      (print "Hola 2")
    )
)


(defun test3(a)
    (when (= a a)
        (print "hola 1")
        (print "hola 2")
    )
)

(test3 4)

(test2 2)

(test 2)


;
;Expresiones para objetos particulares
;Constantes de individuo(aplica en examen), aquellas que identifican al individuo.
;Variables individuales: se determinan a partir de "los, el " o nombres "alicia, ramon, luis"
;Recurso Mnemotecnico: de Ramon seria "R"
;