;Hacer 20 funciones de volumen y de area en lisp.10 area/10 volumen

;Areas-------------------------------------------------
(print "Areas:")
(print "rombo ancho alto")
(print "cuadrado lado")
(print "circulo radio")
(format t "triangulo base altura~%")

(print "Volumenes:")
(print "cubo lado")
(print "ortoedro a b c")
(print "cilindro radio altura")
(print "cono radio altura")
(print "esfera radio")


;Rombo
(defun rombo(x z)
        (print "Area rombo")
        (/ (* x z) 2)

)

;Cuadrado
(defun cuadrado(a)
	(print "Area cuadrado")
	(* a a)

)

;Circulo
(defun circulo(b)
	(print "Area circulo")
	(* (* b 2) 3.14159)

)

;Triangulo
(defun triangulo(c d)
	(print "Area triangulo")
	(/ (* d a) 2)


)

;Rectangulo
(defun rectangulo(e f)
	(print "Area rectangulo")
	(* e f)
)

;Rombiode
(defun romboide(q r)
	(print "Area romboide")
	(* q r)
)

;Trapecio
(defun trapecio(s t u)
	(print "Area trapecio")
	(* (/ (+ s t) 2) a)
)

;Poligono regular
(defun poligonor(v w)
	(print "Area poligono regular")
	(/ (* v w) 2)
)

;Corona circular
(defun coronac(x y)
	(print "Area de la corona circular")
	(* ((- (* x 2) (* y 2)) 3.14159)
)

;Sector circular
(defun sectorc(a n)
	(print "Area sector circular")
	(* (/ (* (* a 2) 3.14159 ) 360) n)
)

;Prisma recto
(defun prisma(a b c)
	(print "Area prisma recto")
	(* (+ b c) a)
)



;Volumenes--------------------------------------------
(defun cubo(g)
	(print "Volumen de un Cubo")
	(* a a a)
)

(defun ortoedro(h i j)
	(print "Volumen ortoedro")
	(* h i j)
)

(defun cilindro(k l)
	(print "Volumen de un Cilindro")
	(* (* (* k 2) l ) 3.14159)
)


(defun esfera(m n)
        (print "Volumen de un Cono")
        (/ (* (* (* m 2) n ) 3.14159) 3)
)

(defun cilindro(o p)
        (print "Volumen de una Esfera")
        (/ (* (* (* k 2) 4 ) 3.14159) 3)
)

(defun paralelepipedo(a b c)
	(print "Volumen del paralelepipedo")
	(* a b c)
)	

(defun piramide(a b)
	(print "Volumen de la piramide")
	(/ (* a b) 3)
)

(defun troncoc(a b c d )
	(print "Volumen del tronco de un cono")
	(/ (* (+ (+ (* a a) (* b b)) (* a b)) c) 3.14159) 3)
)

(defun casquetee(a b)
	(print "Volumen de un casquete esferico")
	(/ (* (- (* b 3) a) (* (* a 2) 3.14159)) 3)
)	

(defun zonaesfera(a b)
	(print "Volumen de una zona esferica")
	(/ (* (+ (+ (* a a) (* (* b b) 3)) (* (* b b) 3)) a 3.14159) 6)
)
