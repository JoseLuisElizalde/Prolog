;Personajes
;Minimo 7 sub-niveles


(defparameter *shingeki_no_kyojin* '(shingeky
                                ;Tiempos en que aun existia eldia
                                (TiempoEldiano
                                    ;Personajes masculinos de eldia
                                    (masculino
                                        ;Simplemente humanos 
                                        (solohumanos
                                        
                                        )
                                        ;titan
                                        (titan
                                            ;Titanes cambiantes
                                            (titan_cambiante
                                                (titan_fundador
                                                    (Ymir_Fritz (La primera entre todos los titante, obtuvo sus poderes de forma desconocida. ))
                                                )
                                            )
                                            ;Titanes incompletos
                                            (titan_incompleto
                                            
                                            )
                                        )
                                    )
                                    ;Personajes femeninos de eldia
                                    (femenino
                                        ;Simplemente humanos 
                                        (solohumanos
                                            (Maria_Fritz (Fue una de las hijas de Ymir Fritz.))
                                            (Rose_Fritz (Fue una de las hijas de Ymir Fritz.))
                                            (Sina_Fritz (Fue una de las hijas de Ymir Fritz.))

                                        )
                                        ;titan
                                        (titan
                                            ;Titanes cambiantes
                                            (titan_cambiante
                                            
                                            )
                                            ;Titanes incompletos
                                            (titan_incompleto
                                            
                                            )
                                        )
                                    )

                                )
                                ;Actualidad en el anime, despues de la crisis de los titanes.
                                (Actualidad
                                (paradis
                                    (masculino
                                        ;Simplemente Humanos---------------------------------------------------------
                                        (solohumanos 
                                            (reconocimiento
                                                (Levi_Ackerman (El hombre mas fuerte de la humanidad))
                                            )

                                            
                                            (Kenny_Ackerman (Ex miembro de la policia militar y capitan del Escuadron de Supresion Anti-Humanos))
                                          
                                            
                                        );End solo humanos
                                        ;Titan------------------------------------------------------------------------
                                        (titan
                                            ;Titanes cambiantes----------------------------------------------
                                            (titan_cambiante
                                                (titan_fundador 
                                                    (Karl_Fritz (Segundo titan fundador))
                                                  
                                                )
                                                (titan_atacante 
                                                    (Eren_Jaeger (Tercer titan atacante y protagonista del anime))
                                                )
                                                (titan_colosal 
                                                    (Armin_Arlert (Segundo titan colosal conocido y amigo de Eren Jaeger))
                                                )
                                                                                    
                                                
                                                
                                                (titan_martillo_de_guerra 
                                                    (Eren_Jaeger (Segundo titan martillo de guerra conocido y protaginista del anime))
                                                )
                                            );End titan cambiante
                                            ;Titan incompleto--------------------------------------
                                            (titan_incompleto
                                                (Rod_Reiss (Padre biologico de Historia y el verdadero rey))
                                               
                                            )
                                        );En titan
                                
                            
                                
                                
                                    );End masculino
                                
                            (femenino
                                ;Solo humanos
                                (solohumanos
                                    (Mikasa_Ackerman (Es la hermana adoptiva Eren Jaeger.))
                                
                                );End solo humanos
                                ;Titan
                                (titan
                                    ;Titanes cambiantes
                                    (titan_cambiante
                                        (titan_fundador
                                            (Frieda_Reiss (Hija mayor de Rod Reiss y media hermana de Historia Reiss))
                                        )
                                    );End titan cambiante
                                    (titan_incompleto
                                                (Sunny_Springer (Hermana menor de Connie Springer))
                                                (Sra_Springer (Madre de Connie Springer))  
                                    );End titan incompleto
                                 );End titan
                            );End femenino
                            );End paradis
                            ;Habitantes de Marley
                            (Marley
                                (masculino
                                    ;Simplemente humanos---------------------------------------------------------------------
                                    (solohumanos
                                        (Willy_Tybur (Lider de la noble familis eldiana Tybur hasta el anio ochosientos cincuenta y cuatro.))
                                      

                                    );End solo humanos
                                    ;titan-----------------------------------------------------------------------------
                                    (titan
                                        ;Titan cambiante----------------------------------------------------------------
                                        (titan_cambiante
                                            (titan_mandibula 
                                                    (Marcel_Galliard (Primer titan mandibula, Companero y amigo de la infancia de reiner braun y bertolt hoover))
                                                
                                            )
                                            (titan_acorazado 
                                                    (Reiner_Braun (Primer titan acorazado conocido y amigo de Bertolt Hoover))
                                            )
                                            (titan_colosal
                                                (Bertolt_Hoover (Primer titan colosal conocido y amigo de reiner braun))
                                            )
                                            (titan_fundador 
                                                    (Grisha_Jaeger (Sexto titan fundador y padre de Eren Jeager))
                                            )
                                            (titan_atacante 
                                                    (Eren_Kruger (Primer titan atacante))
                                                    (Grisha_Jaeger (Segundo titan atacante y padre de eren jaeger))
                                            )
                                            (titan_bestia 
                                                    (Zeke_Jaeger (Primer titan bestia conocido y jefe de guerra de los eldianos que sirven al gobierno de Marley))
                                            )
                                        );End titan cambiante
                                        ;Titan incompleto---------------------------------------------------------------
                                        (titan_incompleto
                                        
                                        
                                        );End titan incompleto
                                    );End titan
                                
                                );End masculino
                                (femenino
                                    ;Simplemente humanos---------------------------------------------------------------------
                                    (solohumanos
                                        (Gabi_Braun (Es una guerrera eldiana al servicio del gobierno de Marley.))
                                    


                                    );End solo humanos
                                    ;titan-----------------------------------------------------------------------------
                                    (titan
                                        ;Titan cambiante----------------------------------------------------------------
                                        (titan_cambiante
                                            (titan_hembra 
                                                    (Annie_Leonhart (Primer titan hembra conocido y amiga de Bertolt Hoover & Reiner Braun))
                                            )
                                            (titan_mandibula
                                                    (Ymir (Soldado graduada de la tropa de reclutas ciento cuatro y ex-miembro de la legion de reconocimiento.))
                                            )
                                            (titan_carguero 
                                                    (Pieck (Guerrera eldiana al servicio del gobierno de Marley y actual poseedora del poder del Titan Carguero.))
                                            )
                                            (titan_martillo_de_guerra
                                                    (Srta_Tybur (Integrante de la noble familia eldiana Tybur))
                                            )
                                        );End titan cambiante
                                        ;Titan incompleto---------------------------------------------------------------
                                        (titan_incompleto
                                            (Dina_Jaeger (Era una descendiente de la familia real y una miembro de los restauradores de Eldia ))
                                        
                                        );End titan incompleto
                                    );End titan
                                
                                );End femenino
                            );End marley
                                );End eldia


)

(defun recorrer (lista)
  (if (equal lista nil)
    (error "No Existe")
  )
  (if( eq (cdadar lista) nil)
   (print '(Tu personaje es, (caadar lista)))
   (progn
     (print '(Tu personaje es, (caar lista)? ))
     (set 'c (read))
     (if (string-equal c "si")
      (recorre (cdar lista))
      (recorre (rest lista))
     )
   )
  )
)