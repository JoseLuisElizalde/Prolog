
;Akinator


;///////////////////////
;///////////////////////Lol
(defparameter *personajes* 
	'(
      (lol

											(mago

												(distancia_si

													(late 	
														(rafaga
																(AP '("Personaje:ZOE"))
														)	
													)
													(medium
														(batalla
																(AP '("Personaje:Vladimir"))
														)
														(rafaga
																(AP '("Personaje:veigar"))
														)

													)
													(early
														(rafaga
																(AP '("Personaje:elise"))
														)
													)

												)
												(distancia_no
													

												)
											
										)
										
											(adc

												(distancia_si

													(late 	
														(batalla
																(AD '("Personaje:vayne"))
														)	
													)
													(medium
														(batalla
																(AD '("Personaje:Twich"))

																(AP '("Personaje:ezreal"))
														)
															
													)
													(early
														(batalla
																(AD '("Personaje:Drave"))
														)
													)

												)
												
											)

											(luchador

													(distancia_no

														(late 	
															(coloso
																	(AD '("Personaje:trundle"))
															)	
														)
														(medium
															(coloso
																	(AP '("Personaje:Signed"))
															)
															(embestidor
																	(AD '("Personaje:Heracim"))
															)
															
														)
														(early
															(embestidor
																	(AD '("Personaje:Vi"))
															)
														)

													)
												

											)
											(asesino

												(distancia_si

														(late 	
															(batalla
																	(AD '("Personaje:kindred"))
															)	
														)
														(medium
															(rafaga
																	(AP '("Personaje:ahri"))
															)
															(embestidor
																	(AD '("Personaje:Heracim"))
															)
															
														)
													

													)
												(distancia_no

														(early 	
															(embestidor
																	(AD '("Personaje:Lee sin"))
															)	
														)
														
													

													)
												
												
												
											)


										)
	  ;;;/////////////////////////Personajes de Harry_Potter
	 (Harry_Potter
		(masculino
			(Gryffindor
				(pelirrojo
					(bueno
						(estudiante
							(mago
								(vivo
									(Ron_Weasley '("Ron El mejor amigo de Harry"))
									(George_Weasley '("George Gemelo de Fred y hermano de Ron"))
									(Bill_Weasley '("Bill Hermano de Ron"))
								)
								(muerto
									(Arthur_Weasley '("Arthur Padre de Ron que trabajaba en el ministerio de magia"))
									(Fred_Weasley '("Fred Gemelo de George y hermano de Ron"))
								)
							)
						)
						(profesor
							(mago
								(muerto
									(Remus_Lupin '("Remus Profesor de defensa y amigo padre de Harry"))
								)
							)
						)
					)
				)
				(pelo_negro
					(bueno
						(estudiante
							(mago
								(vivo
									(Harry_Potter '("Harry Potter Personaje principal de la saga"))
									(Neville_Longbottom '("Neville Amigo de Harry y muy despistado"))
								)
								(muerto
									(Sirius_Black '("Sirius Padrino de Harry y mejor amigo de su padre"))
								)
							)
						)
						(profesor
							(mago
								(muerto
									(Albus_Dumbledore '("Albus Fue director de Hogwarts"))
								)
							)
							(muggle
								(vivo
									(Rubeus_Hagrid '("Hagrid Profesor medio gigante"))
								)
							)
						)
					)
					(malo
						(estudiante
							(mago
								(muerto
									(Peter_Pettigrew '("Peter Amigo de padre de Harry y los traiciono"))
								)
							)
						)
					)
				)
			)
			(Hufflepuff
				(pelo_negro
					(bueno
						(estudiante
							(mago
								(vivo
									(Kinglsey_Shacklebolt '("Kingsley Auror ex estudiante"))
								)
								(muerto
									(Cedric_Diggory '("Cedric Participante del torneo de los tres magos"))
								)
							)
						)
					)
					(malo
						(estudiante
							(mago
								(muerto
									(Gellert_Grindelwald '("Grindelwald Amigo de Dumbledore y despues lo traiciono"))
								)
							)
						)
					)
				)
				
			)
			(Ravenclaw
				(pelirrojo
					(bueno
						(estudiante
							(mago
								(muerto
									(Cornelius_Fudge '("Fudge Era el ministro de magia"))
								)
							)
						)
						(profesor
							(mago
								(muerto
									(Alastor_Moody '("Moody Profesor y auror con un ojo bionico"))
								)
							)
						)
					)
					(malo
						(profesor
							(mago
								(muerto
									(Gilderoy_Lockhart '("Lockhart Profesor de defensa intentó sacrificar a Harry"))
								)
							)
						)
					)
				)
				(pelo_negro
					(bueno
						(estudiante
							(mago
								(vivo
									(Rufus_Scrimgeour '("Scrimgeour Actual ministro de magia"))
								)
								(muerto
									(Sr_Ollivander '("Ollivander Vende varitas y le vendio a Harry su varita"))
								)
							)
						)
					)
					(malo
						(estudiante
							(mago
								(muerto
									(Viktor_Krum '("Krum Participante torneo de Durmstrang se enamora de Hermione"))
								)
							)
						)
					)
				)
				(pelo_amarillo
					(bueno
						(estudiante
							(mago
								(vivo
									(Xenophilius_Lovegood '("Xenophilius Padre de Luna"))
								)
							)
						)
					)
				)
			)
			(Slytherin
				(pelo_negro
					(bueno
						(profesor
							(mago
								(vivo
									(Horace_Slughorn '("Slughorn Ex profesor de pociones"))
								)
								(muerto
									(Severus_Snape '("Snape Profesor de pociones enamorado de la mamá de Harry"))
								)
							)
						)
					)
					(malo
						(estudiante
							(mago
								(muerto
									(Voldemort '("Voldemort El mago mas malo de todos los tiempos"))
								)
							)
						)
						(profesor
							(mago
								(muerto
									(Barty_Crouch '("Crouch Trabajaba para Voldemort"))
								)
							)
						)
					)
				)
				(pelo_amarillo
					(malo
						(estudiante
							(mago
								(vivo
									(Draco_Malfoy '("Draco Hijo de Lucius rico y al final no es malo"))
								)
								(muerto
									(Lucius_Malfoy '("Lucius Padre de Draco rico"))
								)
							)
						)
					)
				)
			)
			(Sin_casa
				(pelo_negro
					(malo
						(familiar
							(muggle
								(vivo
									(Vernon_Dursley '("Vernon Tío adoptivo de Harry"))
									(Dudley_Dursley '("Dudley Primo de Harry despues es bueno"))
								)
							)
						)
						(profesor
							(muggle
								(vivo
									(Argus_Filch '("Filch Cuida Hogwarts"))
								)
							)
						)
					)
				)
			)
		)
		 (femenino
			(Gryffindor
				(pelirrojo
					(bueno
						(estudiante
							(mago
								(vivo
									(Hermione_Granger '("Hermione La mejor amiga de Harry"))
									(Ginny_Weasley '("Ginny Hermana de Ron y esposa de Harry"))
								)
								(muerto
									(Molly_Weasley '("Molly Madre de Ron"))
								)
							)
						)
					)
				)
				(pelo_negro
					(bueno
						(profesor
							(mago
								(muerto
									(Minerva_McGonagall '("McGonagall Profesora de Hogwarts recibió a Harry en su primer día"))
								)
							)
						)
					)
				)
			)
			(Hufflepuff
				(pelirrojo
					(bueno
						(estudiante
							(mago
								(muerto
									(Nymphadora_Tonks '("Tonks fue auror y se caso con Lupin"))
								)
							)
						)
					)
				)
				
				(pelo_amarillo
					(bueno
						(estudiante
							(mago
								(vivo
									(Fleur_Delacour '("Fleur Participante de Beauxbautons se casa con hermano de Ron"))
								)
							)
							
						)
						
					)
					
				)
			)
			(Ravenclaw
				(pelirrojo
					(bueno
						(estudiante
							(mago
								(vivo
									(Madame_Rosmerta '("Rosmerta Atiende el bar las tres escobas"))
								)
							)
							
						)
						(profesor
							(mago
								(vivo
									(Sybill_Trelawney '("Trelawney Profesora de adivinacion hizo profecia de Harry"))
								)
							)
						)
					)
				)
				(pelo_negro
					(bueno
						(estudiante
							(mago
								(vivo
									(Cho_Chang '("Cho Ex novia de Harry jugadora de Quidditch"))
								)
							)
						)
					)
				)
				(pelo_amarillo
					(bueno
						(estudiante
							(mago
								(vivo
									(Luna_Lovegood '("Luna Extravagante amiga de Harry"))
								)
							)
						)
					)
				)
			)
			(Slytherin
				(pelirrojo
					(malo
						(profesor
							(mago
								(muerto
									(Dolores_Umbridge '("Umbridge Profesora de defensa y despues directora"))
								)
							)
						)
					)
				)
				(pelo_negro
					(malo
						(estudiante
							(mago
								(muerto
									(Bellatrix_Lestrange '("Bellatrix Prima de Sirius y seguidora de Voldemort"))
								)
							)
						)
					)
				)
				(pelo_amarillo
					(malo
						(estudiante
							(mago
								(vivo
									(Narcissa_Malfoy '("Narcissa Madre de Draco y esposa de Lucius"))
								)
							)
						)
					)
				)
			)
		)
		(criatura
			(Slytherin
				(pelo_negro
					(malo
						(estudiante
							(mago
								(muerto
									(Fenrir_Greyback '("Greyback Fue un hombre lobo seguidor de Voldemort"))
								)
							)
						)
					)
				)
			)
		)
		(fantasma
			(Ravenclaw
				(pelo_negro
					(bueno
						(estudiante
							(mago
								(muerto
									(Myrtle_La_Llorona '("Asesinada por Tom Riddle, ayuda a Harry"))
								)
							)
						)
					)
				)
			)
			(Sin_casa
				(pelo_negro
					(malo)
						(familiar
							(muggle
								(vivo
									(Petunia_Dursley '("Petunia Tía adoptiva de Harry"))
								)
							)
						)
				)
				(pelo_amarillo
					(malo
						(estudiante
							(muggle
								(vivo
									(Rita_Skeeter '("Rita Periodista de El Profeta"))
								)
							)
						)
					)
				)
			)
		)
	 )	
;///////////////////////Jugadores finalists 2018								
		 (Jugadores_semifinalists
				(belga
					(delantero
						(zurdo
							(mertens '("Mertens jugador del napoli de italia"))
						)
						(derecho

							(piel_blanca

								(hazard '("Hazard delantero del chelsea de italia"))

							)

							(piel_morena

								(estatura_media
									
									(batsuayi '("Batsuayi Delantero del borussia dortmund de alemania"))

								)
								(estatura_alta

									(lukaku '("Lukaku Delantero del manchester united de inglaterra"))
								)


							)

						)

					)
					(medio

						(zurdo
							(chadli '("Chadli Medio del west bromwich de inglatera"))
						)
						(derecho

							(piel_blanca
								(estatura_media

									(liga_inglesa
										(Debruyne '("Debruyne Medio del manchester city de inglaterra"))
									)
									(liga_francesa

										(meunier '("Meunier Medio del paris de Francia"))

									)
								)
							)

							(piel_media

								(estatura_alta
									(liga_inglesa
										(patrocinio_puma
											(fellaini '("Fellaini Medio del manchester united de inglaterra"))
										)
										(patrocinio_adidas

											(dembele '("Dembele Medio del totenham de inglaterra"))	
										)
									)
								)

							)

							(piel_morena
								(lukaku '("Hermano de lukaku juega en la lazio de Italia"))
							)
							
						)
						

					)
					(defensa

						(zurdo

							(piel_blanca

								(estatura_alta
									(alderweireld '("Alderwiereld Defensa del totenham de inglaterra"))
								)

								(estatura_media
									(liga_inglesa

										(patrocinio_adidas
											(vermaelen '("Vermaelen Defensa del barcelona de españa"))
										)
										(patrocinio_puma
											(verthongen '("Verthongen Defensa del totenham de inglarra"))
										)

									)
								)
							)

						)
						(derecho
							(kompany '("Kompany defensa del manchester city de inglaterra"))
						)

						

					)
					(portero

						(zurdo
							(mignolet '("Mignolet Portero del liverpool de inglaterra"))
						)
						(derecho
							(cortouis '("Courtouis Portero del chelsea de inglaterra"))
						)

					)

				)
				(frances

					(delantero
						(zurdo

							(griezzman '("Griezzman delantero del atletico de madrid"))
						)
						(derecho

							(piel_blanca
								(giroud '("Giroud delantero del chelsea de inglaterra"))
							)
							(piel_media
								(fekir '("Fekir Delantero del Lyon de francia"))
							)
							(piel_morena
								(estatura_media
									(liga_francesa
										(patrocinio_puma
											(Mbappe '("Mbappe Jugador del paris de Francia"))
										)
										(patrocinio_adidas
											(Lemar '("Lemmar Jugador del monaco de Francia"))
										)
									)
								)

							)
						)
					)

					(medio

						(derecho

							(piel_blanca
								(thauvin '("Thauvin Medio del marsella de francia"))
							)
							(piel_media
								(tolisso '("Tolisso Medio del bayern munich de alemania"))
							)
							(piel_morena

								(estatura_baja
									(kante '("Kante medio del chelsea de inglaterra"))
								)
								(estatura_media
									(Matuidi '("Matuidi medio de la juventus de italia"))
								)
								(estatura_alta
									(Pogba '("Pogba medio del manchester united de inglaterra"))
								)

							)

						)

					)

					(defensa

						(zurdo
							(piel_blanca
								(lucas '("Lucas defensa del atletico de madrid de españa"))
							)
							(piel_morena
								(umtiti '("Umtiti defensa del barcelona de españa"))
							)	
						)
						(derecho

							(piel_blanca
								(pavard '("pavard defensa del frankfurt de alemania"))
							)
							
							(piel_media
								(varane '("Varane defensa del Real Madrid de españa"))
							)	

						)

					)

					(portero
						(lloris '("Lloris portero del totenham de inglaterra"))
					)

				)
				(croata

					(delantero

						(derecho 

							(piel_blanca
								(rebic '("Rebic jugador del frankfurt de alemania"))
							)
							(piel_media
								(estatura_media
									(liga_italiana
										(patrocinio_adidas
											(manzukic '("Manzukic delantero de la juventus de italia"))
										)
										(patrocinio_nike
											(perisic '("Perisic delantero del inter de milan de italia"))
										)
									)
								)
							)
						)

					)
					(medio

						(derecho

							(piel_blanca

								(estatura_baja
									(modric '("Modric medio del real madrid de inglaterra"))
								)

								(estatura_media
									(liga_española
										(patrocinio_nike 
											(Rakitic '("Rakitic medio del barcelona de españa"))
										)
										(patrocinio_adidas

											(Kovacic '("Kovacic medio del real madrid de españa"))
										)
									)
								)
								(estatura_alta
									(brozovic '("Borozovic medio del iNter de milan de italia"))
								)

							)

						)

					)
					(defensa

						(derecho
							(piel_blanca
								(vidaj '("Vidaj defensa del besiktas de turquia"))
							)
							(piel_media
								(lovren '("Lovren defensa del liverpool de inglaterra"))
							)
						)

					)

				)
				(ingles
						(delantero

							(derecho
								(piel_blanca
									(Kane '("Kane delantero del totenham de inglaterra"))
								)
								(piel_morena
									(Rashford '("Rashford delantero dle manchester united de inglaterra"))
								)
							)

						)
						(medio
							(derecho
								(piel_media
									(delle_alli '("Delle alli medio del totenham de inglaterra"))
								)
								(piel_morena
									(sterling '("Sterling medio del manchester city de inglaterra"))
								)
							)
						)
						(defensa
								(derecho
									(piel_media
										(estatura_media
											(Walker '("Walker Defensa del manchester city de inglaterra"))
										)
										(estatura_alta
											(Smalling '("Smallin defensa del manchester united de inglaterra"))
										)
									)
								)
								(zurdo
									(piel_blanca
										(estatura_media
											(liga_inglesa
												(patrocinio_puma
													(Trippier '(" Trippier Defensa del totenham de inglaterra"))
												)
												(patrocinio_nike
													(Stones '("Stones Defensa del manchester city de inglaterra"))
												)
											)
										)
									)
									(piel_morena
										(Rose '("Rose Defensa del totehnham de inglaterra"))
									)
								)
						)
						(portero
							(pickford '("pickford portero del everton de inglaterra"))
						)
				))

 
;;;/////////////////////////Personajes de Marvel
 (Marvel
	(masculino
		(vivo
			(marvel
				(mutante
		 			(heroe
		 				(animal
		 					(negro
		 						(blackpanter'("Agilidad  Sentidos agudos Velocidad sobrehumana Portador de armas de vibranium"))
		 						)
		 					)
		 				(verde 
		 					(hulk ("hombre fuerte y de color green"))
		 				(garras
		 					(wolverine("Su cuerpo es capaz de recuperarse de cualquier dano fisico Resistencia a la telepatia y algunos tipos de poderes psiquicos"))
		 					)
		 				(silla_ruedas
		 					(profesor("es un mutante con poderes mentales sobrehumanos que le convierten en uno de los telepatas mas poderosos del mundo"))
		 					)		
		 				)
		 				(alas
		 					(angel ("franqueza y rebeldia sobre que le digan que hacer"))
		 				)	
		 				(plateada
		 					(zorroplateada ("Factor curativo artificial y supresor de edad"))
		 				)
		 				(capa
		 					(magneto("Control del magnetismo Creacion de campos de fuerza magneticos ataque o defensa"))
		 				)
		 				(rojo
		 					(ciego
		 						(daredevil("Ciego desde que era joven lucha contra la injusticia de dia como abogado y por la noche como Daredevil"))
		 						)
		 					(feo
		 						(deadpool("Un ex mercenario quien tras haber sido sometido a un cruel experimento adquiere el super poder de sanar rapidamente"))
		 						)

		 					(avedetrueno ("Fuerza y velocidad superhumanas tejido muscular y piel sumamente resistentes"))
		 				)
		 				(arbol
		 					(groot("Succion de madera Resistente al fuego Controlar los arboles Factor de curacion acelerado"))
		 					)
		 				(pelo_largo
		 					(martillo
		 						(thor("Solo el puede empuñar el martillo le envuelve un escudo protector y lleva una armadura de metal y un cinturón de fuerza que incrementa y alimenta su fuerza vital"))
		 						)
		 					(goggles)
		 						(chase("Habil piloto y mecanico Vinculo telepatico a un Deinonychus Guantes y botas de energia"))
		 					)
		 			)
		 			(villano
		 				(enemigo_spiderman
		 					(negro
		 						(venom ("enemigo principal de spiderman con filosos dientes"))
		 						)

		 					(animal
		 						(vuela
		 							(buitre("Atributos fisicos aumentados y vuelo los cuales derivan de su arnes especial"))
		 							)
		 						(gris
		 							(rhino("Posee poderes sobrehumanos como fuerza velocidad resistencia y durabilidad"))
		 							)
		 						(verde
		 							(lagarto("Fuerza resistencia habilidad y reflejos sobrehumanos algunos atributos reptilianos tales como garras dientes afilados piel escamosa"))
		 							)
		 						(aguijon
		 							(escorpion("villano del salvaje oeste que lucho contra Kid Colt"))
		 							)
		 						(varios_colores
		 							(camaleon("Posee la capacidad de imitar cualquier forma humana o cuasihumana Imitador perfecto de voces rostros o gestor peculiares humanos"))
		 							)
		 						(clon
		 							(arania_escarlata("era un clon de Peter Parker creado por el Chacal para luchar y derrotar a SpiderMan"))
		 							)
		 						(cafe
		 							(puma("Sentidos agudos sobrehumanos Fuerza sobrehumana velocidad resistencia agilidad y reflejos"))
		 							)
		 						)
		 					(amarillo_y_rojo
		 						(shocker("Su traje desvia golpes y lo hace dificil de agarrar Sus guantes lanzan ondas sismicas rafagas de aire y altas frecuencias"))
		 						)
		 					(transforma
		 						(hombre_arena ("Su cuerpo es de arena y le proporciona fuerza superhumana"))
		 						)
		 				)
		 				(mas_antiguo_planeta
		 					(apocalipsis("Control auto molecular Fuerza sobrehumana Resistencia sobrehumana Telequinesia Telepatia Teleportacion Factor de curacion acelerado"))
		 					)
		 				(morado
		 					(thanos("Fuerza Velocidad Durabilidad y Longevidad sobrehumana"))
		 					)
		 				(vuela
		 					(verde
		 						(duende("Fuerza sobrehumana, resistencia, durabilidad, agilidad y reflejos debido a la ingestion de la Formula Duende"))
		 						)
		 					(lanza_rayos
		 						(ultron("Inteligencia artificial con cuerpo robotico Resistencia extrema Proyeccion de energia"))
		 						)
		 					(cuernos
		 						(loki("Inteligencia sobrehumana fuerza longevidad magia que incluye proyecciones astrales descargas de energia vuelo teleportacion dimensional y telepatia"))
		 						)
		 					(capa
		 						(verde
		 							(doctordoom("Esta equipado con una armadura que combina lo mejor de la ciencia y la magia otorgandole fuerza sobrehumana y varias capacidades extras"))
		 							)

		 						(roja
		 							(mrsiniestro(" adquirio la habilidad de la inmortalidad Con el pasar de los años Siniestro uso el material genetico de otros mutantes para desarrollar nuevos poderes como telepatia telequinesis fuerza sobrehumana"))
		 							)
		 						)
		 					)
		 				(verde
		 					(cabeza_grande
		 						(lider("Megalomaniaco Antiguo trabajador de una planta de investigacion quimica"))
		 						)
		 					)
		 				(cazador
		 					(kraven("Fuerza y velocidad de las bestias salvajes, aumentadas por ingestion de pociones misticas"))
		 					)

		 			)	
		 		)
				
		 		)
				(dc_comics
					(heroe
						(agua
							(aquaman("Adaptacion acuatica anfibia Telepatia Dominacion psionica de la vida marina"))
							)
						(capa
							(noche
							(batman("caballero de la noche"))
							)
							(amarilla
								(damianwayne("Artista marcial excepcional entrenado por la Liga de Asesinos Acceso a recursos de alta tecnologia"))
								)
							(roja
								(superman("Super fuerza capaz de mover planetas mas grandes y de mayor densidad que la Tierra"))
								)
							(verde
								(mistermiracle("Atributos fisicos sobrehumanos Factor de Curacion Intelecto de nivel Genio Maestro del combate cuerpo a cuerpo"))
								)
							(bbf_batman)
							(robin("joven superheroe compañero de Batman"))
							

							)



					)
					(villano

						(grande
							(gris
								(antimotor("no solo es un enemigo de la Corporacion de los Linterna Verde, sino es un enemigo de todo el Multiverso"))
								)
							(feo
								(bizarro("aparecio por primera vez cuando Superboy fue expuesto a un rayo duplicador"))
								)
							)
						(animal
							(verde
								(gorilagrod("Las habilidades paranormales de Grodd le permiten colocar a otros seres bajo su control mental"))
								)

							)
						(cara_pintada
							(wason("un supervillano enemigo intimo de batman"))
							)
						)

					)


		 	

		)  





		)
    (femenino
			(vivo
				(marvel
					(mutante
						(heroe
							(vuela
								(pelo_gris
									(tormanta("Su domino del clima es uno de los poderes mas espectaculares"))
									)
								(maravillosa
									(mujermaravilla("es una princesa guerrera de las Amazonas y es conocida en su tierra natal como la princesa Diana de Temiscira"))
									)

								)
							(se_puede_ver
								(mujerinvisible("Es capaz de mantener la cordura en la casa de locos que son los 4 Fantasticos"))
								)
							(es_capitana
								(msmarvel("La nueva Capitana Marvel es muy poderosa"))
								)
							(dinosaurio_mascota
								(kittypride ("se la arrebato a la Reina Blanca cuando ésta estaba buscando mutantes para su escuela"))
							)
							)
						)
					)
				)
			) 
		)
;;/////////////////////////Personajes del señor de los anillos
(
                                ;Tiempos en que aun existia eldia
                                (TiempoEldiano
                                    ;Personajes masculinos de eldia
                                    (masculino
                                        ;Simplemente humanos 
                                        (solohumanos
                                        
                                        )
                                        ;titan
                                        (titan
                                            ;Titanes cambiantes
                                            (titan_cambiante
                                                (titan_fundador
                                                    (Ymir_Fritz ("La primera entre todos los titante, obtuvo sus poderes de forma desconocida. "))
                                                )
                                            )
                                            ;Titanes incompletos
                                            (titan_incompleto
                                            
                                            )
                                        )
                                    )
                                    ;Personajes femeninos de eldia
                                    (femenino
                                        ;Simplemente humanos 
                                        (solohumanos
                                            (Maria_Fritz ("Fue una de las hijas de Ymir Fritz."))
                                            (Rose_Fritz ("Fue una de las hijas de Ymir Fritz."))
                                            (Sina_Fritz ("Fue una de las hijas de Ymir Fritz."))

                                        )
                                        
                                    )

                                )
                                ;Actualidad en el anime, despues de la crisis de los titanes.
                                (Actualidad
                                (paradis
                                    (masculino
                                        ;Simplemente Humanos---------------------------------------------------------
                                        (solohumanos 
                                            (reconocimiento
                                                (Levi_Ackerman ("El hombre mas fuerte de la humanidad"))
                                            )

                                            
                                            (Kenny_Ackerman ("Ex miembro de la policia militar y capitan del Escuadron de Supresion Anti-Humanos"))
                                          
                                            
                                        );End solo humanos
                                        ;Titan------------------------------------------------------------------------
                                        (titan
                                            ;Titanes cambiantes----------------------------------------------
                                            (titan_cambiante
                                                (titan_fundador 
                                                    (Karl_Fritz ("Segundo titan fundador"))
                                                  
                                                )
                                                (titan_atacante 
                                                    (Eren_Jaeger ("Tercer titan atacante y protagonista del anime"))
                                                )
                                                (titan_colosal 
                                                    (Armin_Arlert ("Segundo titan colosal conocido y amigo de Eren Jaeger"))
                                                )
                                                                                    
                                                
                                                
                                                (titan_martillo_de_guerra 
                                                    (Eren_Jaeger ("Segundo titan martillo de guerra conocido y protaginista del anime"))
                                                )
                                            );End titan cambiante
                                            ;Titan incompleto--------------------------------------
                                            (titan_incompleto
                                                (Rod_Reiss ("Padre biologico de Historia y el verdadero rey"))
                                               
                                            )
                                        );En titan
                                
                            
                                
                                
                                    );End masculino
                                
                            (femenino
                                ;Solo humanos
                                (solohumanos
                                    (Mikasa_Ackerman ("Es la hermana adoptiva Eren Jaeger."))
                                
                                );End solo humanos
                                ;Titan
                                (titan
                                    ;Titanes cambiantes
                                    (titan_cambiante
                                        (titan_fundador
                                            (Frieda_Reiss ("Hija mayor de Rod Reiss y media hermana de Historia Reiss"))
                                        )
                                    );End titan cambiante
                                    (titan_incompleto
                                                (Sunny_Springer ("Hermana menor de Connie Springer"))
                                                (Sra_Springer ("Madre de Connie Springer"))  
                                    );End titan incompleto
                                 );End titan
                            );End femenino
                            );End paradis
                            ;Habitantes de Marley
                            (Marley
                                (masculino
                                    ;Simplemente humanos---------------------------------------------------------------------
                                    (solohumanos
                                        (Willy_Tybur ("Lider de la noble familis eldiana Tybur hasta el anio ochosientos cincuenta y cuatro."))
                                      

                                    );End solo humanos
                                    ;titan-----------------------------------------------------------------------------
                                    (titan
                                        ;Titan cambiante----------------------------------------------------------------
                                        (titan_cambiante
                                            (titan_mandibula 
                                                    (Marcel_Galliard ("Primer titan mandibula, Companero y amigo de la infancia de reiner braun y bertolt hoover"))
                                                
                                            )
                                            (titan_acorazado 
                                                    (Reiner_Braun ("Primer titan acorazado conocido y amigo de Bertolt Hoover"))
                                            )
                                            (titan_colosal
                                                (Bertolt_Hoover ("Primer titan colosal conocido y amigo de reiner braun"))
                                            )
                                            (titan_fundador 
                                                    (Grisha_Jaeger ("Sexto titan fundador y padre de Eren Jeager"))
                                            )
                                            (titan_atacante 
                                                    (Eren_Kruger ("Primer titan atacante"))
                                                    (Grisha_Jaeger ("Segundo titan atacante y padre de eren jaeger"))
                                            )
                                            (titan_bestia 
                                                    (Zeke_Jaeger ("Primer titan bestia conocido y jefe de guerra de los eldianos que sirven al gobierno de Marley"))
                                            )
                                        );End titan cambiante
                                        ;Titan incompleto---------------------------------------------------------------
                                        (titan_incompleto
                                        
                                        
                                        );End titan incompleto
                                    );End titan
                                
                                );End masculino
                                (femenino
                                    ;Simplemente humanos---------------------------------------------------------------------
                                    (solohumanos
                                        (Gabi_Braun (Es una guerrera eldiana al servicio del gobierno de Marley.))
                                    


                                    );End solo humanos
                                    ;titan-----------------------------------------------------------------------------
                                    (titan
                                        ;Titan cambiante----------------------------------------------------------------
                                        (titan_cambiante
                                            (titan_hembra 
                                                    (Annie_Leonhart (Primer titan hembra conocido y amiga de Bertolt Hoover & Reiner Braun))
                                            )
                                            (titan_mandibula
                                                    (Ymir (Soldado graduada de la tropa de reclutas ciento cuatro y ex-miembro de la legion de reconocimiento.))
                                            )
                                            (titan_carguero 
                                                    (Pieck (Guerrera eldiana al servicio del gobierno de Marley y actual poseedora del poder del Titan Carguero.))
                                            )
                                            (titan_martillo_de_guerra
                                                    (Srta_Tybur (Integrante de la noble familia eldiana Tybur))
                                            )
                                        );End titan cambiante
                                        ;Titan incompleto---------------------------------------------------------------
                                        (titan_incompleto
                                            (Dina_Jaeger ("Era una descendiente de la familia real y una miembro de los restauradores de Eldia "))
                                        
                                        );End titan incompleto
                                    );End titan
                                
                                );End femenino
                            );End marley
                                );End eldia


)


))


;-------------------------------------------------------------------------------
;Funcion de busqueda!

(defun busqueda (list)
  (if (equal list nil)
    (error "      Personaje inexistente, vuelve a intentarlo.")
  )
  (if( eq (cdadar list) nil)
   (princ `(Personaje de, (caadar list)))
   (progn
     (princ `(Personaje de , (caar list)? y/n )  )
     (set 'c (read))
     (if (string-equal c "y")
      (busqueda (cdar list))
      (busqueda (rest list))
     )
   )
  )
)