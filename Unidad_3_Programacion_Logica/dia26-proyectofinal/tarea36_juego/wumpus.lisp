;Programa del capitulo 8 de Land of Lisp

(load "graph-util")

(defparameter *nodos-congestionados-en-la-ciudad* nil)
(defparameter *bordes-congestionados-en-la-ciudad* nil)
(defparameter *visitado-nodos* nil)
(defparameter *nodo-num* 30)
(defparameter *borde-num* 45)
(defparameter *worm-num* 3)
(defparameter *polis-posibilidad* 15)

(defun nodo-random ()
  (1+ (random *nodo-num*)))

(defun borde-pairr (a b)
  (unless (eql a b)
    (list (cons a b) (cons b a))))

(defun hacer-lista-de-bordes ()
  (apply #'append (loop repeat *borde-num*
                        collect (borde-pairr (nodo-random) (nodo-random)))))

(defun bordes-directos (nodo borde-list)
  (remove-if-not (lambda (x)
                   (eql (car x) nodo))
                 borde-list))


;Verifica los nodos conectados
(defun obtener-conectado (nodo borde-list)
  (let ((visitado nil))
    (labels ((traverse (nodo)
                       (unless (member nodo visitado)
                         (push nodo visitado)
                         (mapc (lambda (borde)
                                 (traverse (cdr borde)))
                               (bordes-directos nodo borde-list)))))
            (traverse nodo))
    visitado))


;Conecta las islas 
(defun conectar-con-puentes (islas)
  (when (cdr islas)
    (append (borde-pairr (caar islas) (caadr islas))
            (conectar-con-puentes (cdr islas)))))


;Encuentra las islas 
(defun encontrar-islas (nodos borde-list)
  (let ((islas nil))
    (labels ((encontrar-islas (nodos)
                          (let* ((connected (obtener-conectado (car nodos) borde-list))
                                 (unconnected (set-difference nodos connected)))
                            (push connected islas)
                            (when unconnected
                              (encontrar-islas unconnected)))))
            (encontrar-islas nodos))
    islas))

(defun conectar-todas-las-islas (nodos borde-list)
  (append (conectar-con-puentes (encontrar-islas nodos borde-list)) borde-list))

(defun bordes-to-alist (borde-list)
  (mapcar (lambda (nodo1)
            (cons nodo1
                  (mapcar (lambda (borde)
                            (list (cdr borde)))
                          (remove-duplicates (bordes-directos nodo1 borde-list)
                                             :test #'equal))))
          (remove-duplicates (mapcar #'car borde-list))))

(defun agregar-polis (borde-alist bordes-with-polis)
  (mapcar (lambda (x)
            (let ((nodo1 (car x))
                  (nodo1-bordes (cdr x)))
              (cons nodo1
                    (mapcar (lambda (borde)
                              (let ((nodo2 (car borde)))
                                (if (intersection (borde-pairr nodo1 nodo2)
                                                  bordes-with-polis
                                                  :test #'equal)
                                    (list nodo2 'policias)
                                    borde)))
                            nodo1-bordes))))
          borde-alist))

(defun hacer-bordes-en-la-ciudad ()
  (let* ((nodos (loop for i from 1 to *nodo-num*
                      collect i))
         (borde-list (conectar-todas-las-islas nodos (hacer-lista-de-bordes)))
         (polis (remove-if-not (lambda (x)
                                (zerop (random *polis-posibilidad*)))
                              borde-list)))
    (agregar-polis (bordes-to-alist borde-list) polis)))

(defun vecinos (nodo borde-alist)
  (mapcar #'car (cdr (assoc nodo borde-alist))))

(defun within-one (a b borde-alist)
  (member b (vecinos a borde-alist)))

(defun within-two (a b borde-alist)
  (or (within-one a b borde-alist)
      (some (lambda (x)
              (within-one x b borde-alist))
            (vecinos a borde-alist))))

(defun hacer-nodos-de-la-ciudad (borde-alist)
  (let ((wumpus (nodo-random))
        (glow-worms (loop for i below *worm-num*
                          collect (nodo-random))))
    (loop for n from 1 to *nodo-num*
          collect (append (list n)
                          (cond ((eql n wumpus) '(wumpus))
                                ((within-two n wumpus borde-alist) '(sangre!)))
                          (cond ((member n glow-worms)
                                 '(glow-worm))
                                ((some (lambda (worm)
                                         (within-one n worm borde-alist))
                                       glow-worms)
                                 '(luces!)))
                          (when (some #'cdr (cdr (assoc n borde-alist)))
                            '(sirenas!))))))

(defun juego-nuevo ()
  (setf *congestion-city-bordes* (hacer-bordes-en-la-ciudad))
  (setf *nodos-congestionados-en-la-ciudad* (hacer-nodos-de-la-ciudad *congestion-city-bordes*))
  (setf *player-pos* (encontrar-nodos-vacios))
  (setf *visitado-nodos* (list *player-pos*))
  (dibujar-ciudad)
  (dibujar-progreso))

(defun encontrar-nodos-vacios ()
  (let ((x (nodo-random)))
    (if (cdr (assoc x *nodos-congestionados-en-la-ciudad*))
        (encontrar-nodos-vacios)
        x)))

(defun dibujar-ciudad ()
  (ugraph->png "ciudad" *nodos-congestionados-en-la-ciudad* *congestion-city-bordes*))

(defun mostrando-nodos-de-la-ciudad ()
  (mapcar (lambda (nodo)
            (if (member nodo *visitado-nodos*)
                (let ((n (assoc nodo *nodos-congestionados-en-la-ciudad*)))
                  (if (eql nodo *player-pos*)
                      (append n '(*))
                      n))
                (list nodo '?)))
          (remove-duplicates
            (append *visitado-nodos*
                    (mapcan (lambda (nodo)
                              (mapcar #'car
                                      (cdr (assoc nodo
                                                 *congestion-city-bordes*))))
                            *visitado-nodos*)))))

(defun mostrando-bordes-en-la-ciudad ()
  (mapcar (lambda (nodo)
            (cons nodo (mapcar (lambda (x)
                                 (if (member (car x) *visitado-nodos*)
                                     x
                                     (list (car x))))
                               (cdr (assoc nodo *congestion-city-bordes*)))))
          *visitado-nodos*))


(defun hash-bordes (borde-list)
  (let ((tab (make-hash-table)))
    (mapc (lambda (x)
            (let ((nodo (car x)))
              (push (cdr x) (gethash nodo tab))))
          borde-list)
    tab))

(defun obtener-conectado-hash (nodo borde-tab)
  (let ((visitado (make-hash-table)))
    (labels ((traverse (nodo)
                       (unless (gethash nodo visitado)
                         (setf (gethash nodo visitado) t)
                         (mapc (lambda (borde)
                                 (traverse borde))
                               (gethash nodo borde-tab)))))
            (traverse nodo))
    visitado))

(defun dibujar-progreso ()
  (ugraph->png "mostrando-ciudad" (mostrando-nodos-de-la-ciudad) (mostrando-bordes-en-la-ciudad)))

(defun caminar (pos)
  (direccionar pos nil))

(defun cargar (pos)
  (direccionar pos t))

(defun direccionar (pos cargando)
  (let ((borde (assoc pos (cdr (assoc *player-pos* *congestion-city-bordes*)))))
    (if borde
        (direccionar-nuevo-lugar borde pos cargando)
        (princ "El lugar no existe!"))))

(defun direccionar-nuevo-lugar (borde pos cargando)
  (let* ((nodo (assoc pos *nodos-congestionados-en-la-ciudad*))
         (has-worm (and (member 'glow-worm nodo)
                        (not (member pos *visitado-nodos*)))))
    (pushnew pos *visitado-nodos*)
    (setf *player-pos* pos)
    (dibujar-progreso)
    (cond ((member 'polis borde) (princ "Te encontraste con los policias. Juego Terminado"))
          ((member 'wumpus nodo) (if cargando
                                     (princ "Encontraste el Wumpus!")
                                     (princ "Encontraste al Wumpus.")))
          (cargando (princ "Perdiste tu ultima bala. Juego Terminado."))
          (has-worm (let ((new-post (nodo-random)))
                      (princ "Te encontraste con un miembro de la pandilla Gloworm.! Ahora estas en ")
                      (princ new-post)
                      (direccionar-nuevo-lugar nil new-post nil))))))
