Sacar tipo de predicados(monario,binario,ternario), constantes de individuo y variables de individuo, y 
que tipo de cuantificadores hay(existencial,universal) y reescribirlos si se puede(sin cambiarle el contexto).
Constantes
Variables
Funciones de aridad
Predicados monadicos
Ejm:
    P(x): x es estudiante de informatica
    Q(x): x es aficionado a la logica

Predicados de aridad
    Hay una relacion entre dos objetos
Ejm:
    R(x,y): x es amigo de y
cuantificadores
 

Simbolos
--------------
∃
∀
¬
∧



Despues, formalizar.

1. 
Laura quiere a Javier,  
    Binario; Constantes: "laura" y "javier";No tiene variables; No tienen propiedad; Relacion: "quiere a" 
    Q(a,b)    ; a=laura, b=javier, Q= quiere; Formalizacion: Qab
Laura se dedica a la informatica.
    Monario; Constantes: "laura" ; Relacion: decicada a 
    I(a) ; a=laura I=Es Informatica
Ningun medico se dedica a la informatica. 
    Monario; 
    Vx(Mx --> ~I(x))
Solo los medicos quieren a los enfermos.
    Binario ; 



2.
Todos los perros persiguen algun gato. 

Hay gatos que persiguen algun perro. 
Un gato bien educado jamas persigue perros.
A los perseguidores de gatoss no les gustan los gato mal educados. 
Por tanto, algunos gatos no gustan a ningun perro.




3. 
Todos los colibries tienen vivos colores. 
Ningun pajaro de gran tamaño se alimenta de miel. 
Los pajaros que no se alimentan de miel tienen colores apagados.



4. 
Ningun enamorado suspira. 
Todos los deprimidos suspiran.
En consecuencia, nadie que este deprimido esta enamorado.

S(x): x suspira
Constantes:   
Variables: x:Enamorado, y:Deprimido
reescritura: 
    Para todo x, si x no esta enamorado, entonces no suspira.
    Para todo y, si y esta deprimido, entonces suspira.
    Por lo tanto, si x no esta deprimido, entonces y no esta enamorado  
    

    
    

5. Ningun individuo que siga estrictamente las leyes de la logica tiene
corazon. Todos los extraterrestres siguen estrictamente las leyes de la 
logica. Por tanto, los extraterrestres carecen de corazon.



