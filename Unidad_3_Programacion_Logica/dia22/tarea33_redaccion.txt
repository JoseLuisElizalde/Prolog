Redaccion acerca de que se necesitaria  si soy una empresa y necesito desarrollar un sistema que reconozca el estado 
de animo a traves de las publicaciones que se tengan en las redes sociales existentes. 

-----------------------------------------------------------------------------------------------------------------------


Que herramientas
En base a la evaluación de emociones se podría comenzar con la valoración de las mismas, para ello se podría utilizar como
premisa la "Escala de valoración del estado de animo EVEA", la cual tiene un aproximado de la media de valoración normal 
de las emociones provistas por sus anteriores investigaciones.
Tales investigaciones pueden ser de gran ayuda para proveer de una base de datos solida al algoritmo del cual se dispondra
para evaluar las expersiones y dadas estas poder comenzar con el periodo de entrenamiento y validación de las expresiones, 
tal como se ve ahora: 
    Preriodo de entrenamiento: 
        Modelo, conocimiento/suposiciones a priori.
        Estimación de los parametros del clasificador.
        Muestra de entrenamiento.
    Validación
        Muestra de validación.
        Predicción basada en el clasificador. 
        Valores predichos.

 

Que software
    En cuanto al software utilizable, más bien se podría utilizar la información disponible en estadisticas de la red 
    social a utilizar e información disponible online acerca de las tendencias usuales de cada usuario que itiliza dicha
    red social.

Que algoritmos


Además de poder utilizar también algoritmos de busqueda de patrones, o algunos recursos extra que podrían hacer este 
método aún más eficaz, tales como:
    Redes Neuronales
    Máquina de vector de soporte
    Árboles de decisión
    Redes Bayesianas    
    Algoritmos de votación: Bagging, Boosting
    Modelos Markovianos Ocultos
    Métodos no supervisados 
    Combinación de información
    Regresión logistica
    Clasificadores: Tales como IB1 e IBk
    Listas de decisión: Tales como PART 


Que entradas tendria 
    En base de la "Mineria de opiniones", la cual es una derivación de las mineria de datos nos ayuda a que la generación
    de texto en las redes sociales ayude a las masas a verter opiniones, las tendencias que tienen estas y que tipo de 
    sentimientos se manifiestan a través de ellos. Extrayendo los datos en base al texto, imagenes, videos publicados en las 
    redes disponibles.

    -Identificando palabras relacionadas y expresiones.
    -

    -Reglas de contexto.