;Personajes
;Minimo 7 sub-niveles


(defparameter *shingeki_no_* '(

                                (lugar_desconodico
                                
                                )
                                (Eldia
                                (paradis
                                    (masculino
                                        ;Simplemente Humanos---------------------------------------------------------
                                        (solohumanos 
                                            (Levi_Ackerman (El hombre mas fuerte de la humanidad))
                                            (Kenny_Ackerman (Ex miembro de la policia militar y capitan del Escuadron de Supresion Anti-Humanos))
                                            (Erwin_Smith (13o comandante de la Legion de Reconocimiento))
                                            (Jean_Kirstein (Graduado de la Tropa de Reclutas del Batallon ciento cuatro y sexto puesto.))
                                            (Fritz (Rey falso y sustituto de Rod Reiss))
                                            (Keith_Shadis (Instructor en jefe de la tropa de reclutas ciento cuatro y ex-comandante de la Legion de Reconocimiento.))
                                            (Sr_Ackerman (Padre de Mikasa Ackerman y esposo de la Sra. Ackerman.))
                                            (Connie_Springer (Miembro de la Legion de Reconocimiento y octavo puesto de su generacion.))
                                            (Dimo_Reeves (Comerciante y propietario de un negocio que llevaba su apellido.))
                                            (Marco_Bott (Miembro de la tropa de reclutas ciento cuatro y septimo puesto de su generacion.))
                                            (Djel_Sanes (Miembro de la policia militar.))
                                            (Abuelo_Ackerman (Abuelo de Kenny y bisabuelo de Levi Ackerman, tenia el conocimiento de porque los Ackerman eran perseguidos.))
                                            (Sr_Smith (Profesor y padre del comandante Erwin Smith))
                                            (Darius_Zackly (Jefe de las tres divisiones del ejercito: La legion de reconocimiento, las tropas estacionarias y la policia militar.))
                                            (Hannes (Capitan de las Tropas Estacionatias y gran amigo de la familia jaeger.))
                                            (Mike_Zacharius (Miembro de la legion de Reconocimiento y segundo soldado mas fuerte despues de Levi.))
                                            (Dot_Pixis (Comandante de las Tropas Estacionarias.))
                                            (Nick (Miembro de la secta religiosa. ))
                                            ()

                                        );End solo humanos
                                        ;Titan------------------------------------------------------------------------
                                        (titan
                                            ;Titanes cambiantes----------------------------------------------
                                            (titan_cambiante
                                                (titan_fundador 
                                                    (Karl_Fritz (Segundo titan fundador))
                                                    (Padre_de_rud_y_uri_reiss (Tercer titan fundador))
                                                    (Uri_Reiss (Cuarto titan fundador))
                                                    (Grisha_Jaeger (Sexto titan fundador y padre de Eren Jeager))
                                                    (Eren_Jaeger (Septimo titan fundador y protagonista del anime))
                                                )
                                                (titan_atacante 
                                                    (Eren_Kruger (Primer titan atacante))
                                                    (Grisha_Jaeger (Segundo titan atacante y padre de eren jaeger))
                                                    (Eren_Jaeger (Tercer titan atacante y protagonista del anime))
                                                )
                                                (titan_colosal 
                                                    (Bertolt_Hoover (Primer titan colosal conocido y amigo de reiner braun))
                                                    (Armin_Arlert (Segundo titan colosal conocido y amigo de Eren Jaeger))
                                                )
                                                (titan_acorazado 
                                                    (Reiner_Braun (Primer titan acorazado conocido y amigo de Bertolt Hoover))
                                                )                                    
                                                (titan_bestia 
                                                    (Zeke_Jaeger (Primer titan bestia conocido y jefe de guerra de los eldianos que sirven al gobierno de Marley))
                                                )
                                                (titan_mandibula 
                                                    (Marcel_Galliard (Primer titan mandibula, Companero y amigo de la infancia de reiner braun y bertolt hoover))
                                                    (Porco_Galliard (Tercer titan mandibula conocido y guerrero eldiano al servicio del gobierno de Marley))
                                                
                                                )
                                                (titan_martillo_de_guerra 
                                                    (Eren_Jaeger (Segundo titan martillo de guerra conocido y protaginista del anime))
                                                )
                                            );End titan cambiante
                                            ;Titan incompleto--------------------------------------
                                            (titan_incompleto
                                                (Rod_Reiss (Padre biologico de Historia y el verdadero rey))
                                                (Sra_Springer (Madre de Connie Springer))  
                                                (Sr_Springer (Padre de Connie Springer))
                                                (Sunny_Springer (Hermana menor de Connie Springer))
                                            
                                            )
                                        );En titan
                                
                            
                                
                                
                                    );End masculino
                                
                            (femenino
                                (titan_cambiante
                                    (titan_acorazado 
                                        (Annie_Leonhart (Primer titan acorazado conocido y amigo de Bertolt Hoover))
                                    ) 
                                    
                                 )
                            );End femenino
                            );End paradis
                            (Marley
                                (masculino
                                    ;Simplemente humanos---------------------------------------------------------------------
                                    (solohumanos
                                        (Willy_Tybur (Lider de la noble familis eldiana Tybur hasta el anio ochosientos cincuenta y cuatro.))

                                    );End solo humanos
                                    ;titan-----------------------------------------------------------------------------
                                    (titan
                                        ;Titan cambiante----------------------------------------------------------------
                                        (titan_cambiante
                                        
                                        );End titan cambiante
                                        ;Titan incompleto---------------------------------------------------------------
                                        (titan_incompleto
                                        
                                        
                                        );End titan incompleto
                                    );End titan
                                
                                );End masculino
                                (femenino
                                
                                
                                );End femenino
                            );End marley
                                );End eldia


)